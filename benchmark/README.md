# Benchmarks

## Dardel

### Ground state time for SCF cycle step

- Note that the values obtained with the benchmark script, where
  each scf step is called separately, are 3 times as good as when running a
  normal ground state calculation. I think the lack of density mixing accounts
  for this
- Use as little domain parallelization as possible. For 2000 electrons and
  4M grid points 2 works.
- 25% or so decrease in wall time by using 2 nodes, domain=2, no further
  decrease thereafter.

![GS results](gs.png)

### Time propagation time per step

- Use a few domains as possible, 2 works in this case
- Scales better than GS to several nodes.
- Example 3000 time steps:
  - 1 node 20s/step -> 16 wall hours (3000 core-hours) total
  - 4 nodes 10s/step -> 8 wall hours (6000 kcore-hours) total

![TD results](td.png)
