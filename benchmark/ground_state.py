import numpy as np

from ase.io import read
from ase.parallel import parprint
from ase.units import Ha

from gpaw import GPAW, mpi, Mixer, FermiDirac, KohnShamConvergenceError
from gpaw.poisson import PoissonSolver
from gpaw.poisson_moment import MomentCorrectionPoissonSolver


def initialize_calculation():
    from os import getenv

    h = 0.2  # Grid spacing

    mixbeta = float(getenv('GPAW_MIXBETA', '0.1'))
    mixw = float(getenv('GPAW_MIXW', '50.0'))
    mixold = int(getenv('GPAW_MIXOLD', '5'))

    nb = int(getenv('GPAW_NBANDS', '-300'))
    ncb = int(getenv('GPAW_NCONVBANDS', '-150'))

    atoms = read('struct.xyz', parallel=False)

    center = atoms.get_positions()[:201].mean(axis=0)
    parprint(f'Center of NP is at {center}', flush=True)

    moment_corrections = [dict(moms=range(1+3), center=center)]
    poissonsolver = MomentCorrectionPoissonSolver(
            PoissonSolver(),
            moment_corrections=moment_corrections)
    occ = FermiDirac(0.05)

    parprint(f'World size {mpi.world.size}')
    domain = getenv('GPAW_DOMAIN_SIZE', 2)
    par_opts = {"sl_auto": True, "domain": domain, "augment_grids": True}

    # Ground-state calculation
    mode = 'lcao'

    calc_kwargs = {}
    calc_kwargs['setups'] = {'Ag': '11'}
    calc_kwargs['basis'] = {'Ag': 'pvalence.dz', 'C': 'dzp', 'O': 'dzp'}
    calc_kwargs['poissonsolver'] = poissonsolver
    calc_kwargs['mixer'] = Mixer(beta=mixbeta, nmaxold=mixold, weight=mixw)
    calc_kwargs['occupations'] = occ
    calc_kwargs['parallel'] = par_opts

    calc_kwargs['convergence'] = {'density': 1e-8}
    calc_kwargs['nbands'] = nb
    calc_kwargs['convergence']['bands'] = ncb

    calc = GPAW(atoms=atoms, maxiter=150,
                mode=mode, h=h, xc='GLLBSC', txt='/dev/stdout', **calc_kwargs)
    calc.atoms = atoms
    atoms.set_calculator(calc)

    return calc


def ground_state():
    calc = initialize_calculation()
    atoms = calc.atoms

    try:
        atoms.get_potential_energy()
    except KohnShamConvergenceError:
        if False:  # Take additional step, saving the state before and after
            calc.write('gs-1.gpw', mode='all')
            next(calc.scf.irun(calc.wfs, calc.hamiltonian,
                               calc.density, calc.log, calc.call_observers))
            calc.write('gs-2.gpw', mode='all')
            return

        parprint('Convergence error!')
        return

    calc.write('gs.gpw', mode='all')

    parprint('Converged ground state, saved to gs.gpw', flush=True)

    # Save DOS and parameters
    save_dos(calc)

    mpi.world.barrier()


def save_dos(calc):
    wfs = calc.wfs
    k, spin = 0, 0

    # DOS
    eig_n = wfs.collect_eigenvalues(k=k, s=spin)*Ha - calc.get_fermi_level()

    cellpar = calc.atoms.cell.cellpar()
    save = {'eig_n': eig_n,
            'fermilevel': 0, 'vacuumlevel': 0 - calc.get_fermi_level(),
            'cell_lengths': cellpar[0:3],
            'cell_angles': cellpar[3:6],
            'pot_energy': calc.get_potential_energy(),
            }
    parprint('Saving DOS dos.npz', flush=True)
    np.savez_compressed('dos.npz', **save)


def save_info_from_restart():
    calc = GPAW('gs.gpw')
    save_dos(calc)


if __name__ == "__main__":
    ground_state()
