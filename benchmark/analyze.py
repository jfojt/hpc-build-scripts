import sys

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker

from json import loads


if len(sys.argv) < 1:
    print(f'Usage: {sys.argv[0]} LOGFILE [SAVEFIG]')
    sys.exit(1)

logfile = sys.argv[1]

if len(sys.argv) < 2:
    savefig = None
else:
    savefig = sys.argv[2]

with open(logfile, 'r') as fp:
    jsonstr = fp.read()

# Format as proper JSON
jsonstr = jsonstr[:-2].rstrip(',\n')
jsonstr = f'[{jsonstr}]'

all_data = loads(jsonstr)

domain_d = np.array(sorted(list({data['DOMAIN'] for data in all_data})))
nodes_n = np.array(sorted(list({data['NODES'] for data in all_data})))

node_colors_n = [cm.get_cmap('tab10')(f) for f in np.linspace(0, 1, 10)]

gridspec_kw = dict(left=0.12, top=0.98, right=0.98, bottom=0.14,
                   wspace=0, hspace=0.1)
fig, axes = plt.subplots(2, 1, figsize=(5.9, 5), dpi=200,
                         gridspec_kw=gridspec_kw)

ax_wall, ax_cpu = axes

for data in all_data:
    domain, nodes, tasks = data['DOMAIN'], data['NODES'], data['NTASKS']
    d = np.argmax(domain_d == domain)
    n = np.argmax(nodes_n == nodes)
    res = data['result']

    plot_kw = dict(lw=2, color=node_colors_n[n])
    if len(res) == 0:
        ax_wall.plot(d, 0, 'x', **plot_kw)
        ax_cpu.plot(d, 0, 'x', **plot_kw)
        continue

    timing_t = np.array(res['timing_t'])
    ax_wall.errorbar(d, timing_t.mean(), yerr=timing_t.std(),
                     **plot_kw, capsize=4)
    ax_cpu.errorbar(d, timing_t.mean()*tasks/3600,
                    yerr=timing_t.std()*tasks/3600,
                    **plot_kw, capsize=4)

ax_wall.set_ylabel('Wall time (s)')
ax_cpu.set_ylabel('CPU time (core-hours)')


if False:
    def format_wall_seconds(seconds, x):
        import time

        return time.strftime('%M:%S', time.gmtime(seconds))

    wall_formatter = mticker.FuncFormatter(format_wall_seconds)
    ax_wall.yaxis.set_major_formatter(wall_formatter)

for ax in axes:
    ax.set_xticks(range(len(domain_d)))

for ax in axes[:-1]:
    ax.set_xticklabels([])

for ax in [ax_wall]:
    for n, nodes in enumerate(nodes_n):
        ax.plot([], [], '-', color=node_colors_n[n], label=f'{nodes} nodes')
    ax.legend()

axes[-1].set_xlabel('# domains')
axes[-1].set_xticklabels([f'{domain:.0f}' for domain in domain_d])

if savefig is None:
    plt.show()
else:
    fig.savefig(savefig)
