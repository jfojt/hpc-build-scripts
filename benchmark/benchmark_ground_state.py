import numpy as np

from gpaw import GPAW, mpi


def benchmark_ground_state():
    from json import dumps
    from time import time
    from os import getenv

    domain = int(getenv('GPAW_DOMAIN_SIZE', 2))
    par_opts = {"sl_auto": True, "domain": domain, "augment_grids": True}

    calc = GPAW('gs.gpw', parallel=par_opts, txt='/dev/null')

    # Initialize everything
    calc.calculate(properties=['energy'], system_changes=[])

    t0 = time()
    Nsteps = 10
    timing_t = np.zeros((Nsteps))

    for t in range(Nsteps):
        next(calc.scf.irun(calc.wfs, calc.hamiltonian,
                           calc.density, calc.log, calc.call_observers))
        t1 = time()
        timing_t[t] = t1 - t0
        t0 = t1

    if mpi.world.rank == 0:
        save = dict(par_opts=par_opts, timing_t=timing_t.tolist())
        print(dumps(save))
    mpi.world.barrier()


if __name__ == "__main__":
    benchmark_ground_state()
