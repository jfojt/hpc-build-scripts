from gpaw import mpi
from gpaw.lcaotddft import LCAOTDDFT

import numpy as np


def benchmark_time_propagation(continuing=False):
    from json import dumps
    from time import time
    from os import getenv

    domain = int(getenv('GPAW_DOMAIN_SIZE', 2))
    par_opts = {"sl_auto": True, "domain": domain, "augment_grids": True}
    calc = LCAOTDDFT('gs.gpw', txt='/dev/null', parallel=par_opts)

    dt = 10
    kick = 1e-5*np.array([0, 0, 1])

    calc.absorption_kick(kick)

    t0 = time()
    Nsteps = 10
    timing_t = np.zeros((Nsteps))

    for t in range(Nsteps):
        calc.propagate(dt, 1)
        t1 = time()
        timing_t[t] = t1 - t0
        t0 = t1

    if mpi.world.rank == 0:
        save = dict(par_opts=par_opts, timing_t=timing_t.tolist())
        print(dumps(save))
    mpi.world.barrier()


if __name__ == "__main__":
    benchmark_time_propagation()
