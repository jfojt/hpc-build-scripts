workdir="scratch"
jobscript="job.sh"

if [ $# -lt 1 ]; then
    echo -e "$0 args [# nodes] [Time limit]"
    exit 1
fi

args="$1"

nnodes="${2:-1}" # Number of nodes, default 1

tlimit="${3:-01:00:00}" # Time limit

ntasks_per_node=128
if [ $# -gt 3 ]; then
    ntasks_per_node="$4"
fi

mkdir -p "$workdir"

echo "#!/bin/bash -l
#SBATCH -A snicXXXX-Y-ZZZ
#SBATCH -J my-job
#SBATCH -t 01:00:00
#SBATCH -p main

# These environment variables are convenient if we want to know how much time is left on the job

export START_TIME=\$(date +%s.%3N)
export TIME_LIMIT=\$(squeue -h -o %l -j \$SLURM_JOB_ID)

module purge
module load gpaw/parallel

srun gpaw python $args

" > "$workdir/$jobscript"

jobid=$(sbatch --job-name="GPAW $args" --nodes=$nnodes --ntasks-per-node=$ntasks_per_node \
    --error="$workdir/%j.e" --output="$workdir/%j.o" -t $tlimit \
    --mem=0 --parsable "$workdir/$jobscript")

echo "Submitted job $jobid"
