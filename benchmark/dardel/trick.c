/*
 * What is this?
 * - See https://documentation.sigma2.no/jobs/mkl.html
 *   and https://danieldk.eu/Posts/2020-08-31-MKL-Zen.html
 *
 * How to compile it?
 * - cc -shared -fPIC -o libtrick.so trick.c
 *
 * What then?
 * - In job script
 *   LD_PRELOAD=libtrick.so
 *
 * Does it improve performance?
 * - I didn't see an effect in 5 minutes of testing
 */

int mkl_serv_intel_cpu_true() {
    return 1;
}
