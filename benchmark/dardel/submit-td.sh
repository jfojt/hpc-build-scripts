#!/bin/bash -l
#SBATCH -A snicXXXX-Y-ZZZ
#SBATCH -J "Benchmark time propagation"
#SBATCH -o "Benchmark_td_%j.o"
#SBATCH -e "Benchmark_td_%j.e"
#SBATCH -t 01:00:00
#SBATCH --ntasks-per-node 128
#SBATCH --mem 0
#SBATCH --nodes 1
#SBATCH -p main

# These environment variables are convenient if we want to know how much time is left on the job

export START_TIME=$(date +%s.%3N)
export TIME_LIMIT=$(squeue -h -o %l -j $SLURM_JOB_ID)

module purge
module load gpaw/parallel

bench_log="bench_nodes_${SLURM_NNODES}_td.log"

for domain in $(seq $SLURM_NTASKS); do
    if (( $SLURM_NTASKS % $domain != 0 )); then
        continue
    fi
    echo Domain $domain

    out="$(GPAW_DOMAIN_SIZE=$domain srun gpaw python benchmark_time_propagation.py)"
    status=$?

    if [ "$out" == "" ]; then
        out="{}"
    fi

    echo "{
    \"NODES\": $SLURM_NNODES,
    \"NTASKS\": $SLURM_NTASKS,
    \"NTASKS_PER_NODE\": $SLURM_NTASKS_PER_NODE,
    \"DOMAIN\": $domain,
    \"status\": $status,
    \"result\": $out
}," >> "$bench_log"

done
