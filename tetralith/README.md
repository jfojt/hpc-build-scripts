# Tetralith

Contents of this document are:

- Installation of [GPAW](https://wiki.fysik.dtu.dk/gpaw/index.html)
- Installation of [git LFS](https://git-lfs.github.com/)
- Notes on Tetralith

## GPAW Installation

It seems like on Tetralith, the same compiled version of GPAW works on both the
compute nodes and login nodes.

The FFTW module is loaded via the module system, but the libxc is statically linked
to and does not need loading.

The script in this directory installs GPAW in a Conda environment.
In principle module files are not needed (it is enough to load
the conda environment), but can be easier to use than conda activate.

Installation in home directory: (recommended to replace the home directory ~
by a path in your nobackup or project directory):

**Note:** Conda will ask for confirmation before installing, so paste each line
below separately and respond to the prompt in the terminal

```
mkdir ~/git
git clone https://gitlab.com/ase/ase ~/git/ase
git clone https://gitlab.com/gpaw/gpaw ~/git/gpaw
./run-me.sh conda ~/my-conda-env
./run-me.sh gpaw-data ~/gpaw-setups ~/module/gpaw-setups.lua
./run-me.sh ase ~/git/ase ~/my-conda-env
./run-me.sh gpaw ~/git/gpaw ~/my-conda-env ~/module/gpaw/my-gpaw.lua
echo "module use ~/module" >> ~/.bashrc
```

## Git LFS installation

The installation of Git LFS is pretty straight forward, as nothing needs to be
compiled. The installation tool will download and extract the necessary files to
the requested location.

The following downloads and installs Git LFS to `~/git-lfs` and creates the module
file `~/module/git-lfs.lua`. Adjust to your own needs

```
./run-me.sh git-lfs ~/git-lfs ~/module/git-lfs.lua
echo "module use ~/module" >> ~/.bashrc
```

# Notes on Tetralith

## Intel build environment

Some information is displayed when loading the module

```
module load buildenv-intel/2023a-eb
```

Importantly: **NOTE: You shoud never load build environments inside submitted jobs.**
