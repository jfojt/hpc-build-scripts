"""User provided customizations.

Here one changes the default arguments for compiling _gpaw.so.

Here are all the lists that can be modified:

* libraries
  List of libraries to link: -l<lib1> -l<lib2> ...
* library_dirs
  Library search directories: -L<dir1> -L<dir2> ...
* include_dirs
  Header search directories: -I<dir1> -I<dir2> ...
* extra_link_args
  Arguments forwarded directly to linker
* extra_compile_args
  Arguments forwarded directly to compiler
* runtime_library_dirs
  Runtime library search directories: -Wl,-rpath=<dir1> -Wl,-rpath=<dir2> ...
* extra_objects
* define_macros

The following lists work like above, but are only linked when compiling
the parallel interpreter:

* mpi_libraries
* mpi_library_dirs
* mpi_include_dirs
* mpi_runtime_library_dirs
* mpi_define_macros

To override use the form:

    libraries = ['somelib', 'otherlib']

To append use the form

    libraries += ['somelib', 'otherlib']
"""
# flake8: noqa

library_dirs = []
libraries =[]

# The strategy for Tetralith is to statically link or set rpaths everything, so that
# no other modules need to be loaded at runtime.
# elpadir = '/software/sse/manual/ELPA/2018.05.001/g73/oblas034/ompi312'
libxcdir = '/software/sse/manual/libxc/4.0.4/g73/nsc2/'
libvdwxcdir = '/software/sse/easybuild/prefix/software/libvdwxc/0.4.0-foss-2018a-nsc1'

# -static-intel : Statically links some mkl stuff. Increases .so size by 500kB.
#                 Otherwise we need to link to libffi.so
libraries += ['mkl_core', 'mkl_cdft_core', 'mkl_intel_lp64', 'mkl_sequential',
              'mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64',
              'readline']
extra_compile_args += ['-O3', '-fPIC']
extra_link_args += [f'-qmkl=sequential', '-static-intel']

compiler = 'mpiicc'  # Not the double i here; mpi i(intel) cc
mpicompiler = 'mpiicc'
mpilinker = 'mpiicc'
# platform_id = ''

# FFTW3:
fftw = True
if fftw:
    libraries += ['fftw3xc_intel_pic']  # Provided by intel MKL

# It seems not to matter whether this is set or not
# define_macros += [('GPAW_NO_UNDERSCORE_BLAS', '1')]
scalapack = True

# Use Elpa (requires ScaLAPACK and Elpa API 20171201):
# - dynamic linking with explicitly set rpath
elpa = False
if elpa:
    libraries += ['elpa']
    library_dirs += [f'{elpadir}/lib']
    extra_link_args += [f'-Wl,-rpath={elpadir}/lib']
    include_dirs += [f'{elpadir}/include/elpa-2018.05.001']

# LibXC:
# - static linking
include_dirs += [f'{libxcdir}/include']
extra_link_args += [f'{libxcdir}/lib/libxc.a']
if 'xc' in libraries:
    libraries.remove('xc')


# libvdwxc:
# - dynamic linking with explicitly set rpath
libvdwxc = False
if libvdwxc:
    extra_link_args += [f'-Wl,-rpath={libvdwxcdir}/lib']
    library_dirs += [f'{libvdwxcdir}/lib']
    include_dirs += [f'{libvdwxcdir}/include']
    libraries += ['vdwxc']
