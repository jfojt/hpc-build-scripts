#!/bin/bash

procedure="$1"

die() {
    echo $@
    exit 1
}

get_pyver() {
    # This is the same as in config.py in GPAW repo, but it does not always match
    # what is actually installed it seems. In particular it does not get the -cpython
    conda_prefix_dir="$1"
    pycmd="""
import sys
import sysconfig

plat = sysconfig.get_platform()
vi = sys.version_info  # sysconfig.get_python_version seems to give same as vi[0].vi[1]

print(f'{plat}-{vi[0]}.{vi[1]}', end='')
"""
    conda run --prefix "$conda_prefix_dir" python -c "$pycmd"
}

get_pylibdir() {
    conda_prefix_dir="$1"
    pycmd="""
import sysconfig
from distutils.sysconfig import get_python_version

plat = sysconfig.get_platform()
so_abi = sysconfig.get_config_var('SOABI')  # Example cpython-39-x86_64-linux-gnu
libdir_abi = '-'.join(so_abi.split('-')[:2])  # Example cpython-39

# print(f'{plat}-{get_python_version()}')  # This works with intel's version of numpy from conda/intel
print(f'{plat}-{libdir_abi}')
"""
    conda run --prefix "$conda_prefix_dir" python -c "$pycmd"
}


get_py_so_abi() {
    conda_prefix_dir="$1"
    pycmd="""
import sysconfig

so_abi = sysconfig.get_config_var('SOABI')  # Example cpython-39-x86_64-linux-gnu
print(so_abi)
"""
    conda run --prefix "$conda_prefix_dir" python -c "$pycmd"
}

ml_conda() {
    module load Anaconda/2023.09-0-hpc1
}

ml_conda_buildenv() {
    module load Anaconda/2023.09-0-hpc1
    module load buildenv-intel/2023a-eb  # Provides mpiicc (note double i; mpi i(intel)cc)
    # Don't load the modules for FFTW. We will instead statically link to it by providing the path
    # directly to siteconfig.py
}


create_conda_env() {
    ml_conda

    echo Creating conda environment
    conda_prefix_dir="$(realpath "${1:-./conda-gpaw-env}")"
    conda_opts="${@:2}"
    conda_opts="${conda_opts:-python=3.11 'numpy>=1.26' blas=*=*mkl scipy>=1.7.3}"

    echo conda create --prefix "$conda_prefix_dir" $conda_opts
    conda create --prefix "$conda_prefix_dir" $conda_opts
}

install_gpaw_data() {
    location="$(realpath "${1:-./gpaw-setups}")"
    pvalence_basis="https://wiki.fysik.dtu.dk/gpaw-files/gpaw-basis-pvalence-0.9.20000.tar.gz"
    setups="https://wiki.fysik.dtu.dk/gpaw-files/gpaw-setups-0.9.20000.tar.gz"
    mkdir -p "$(dirname "$2")"
    module_file="$(realpath "${2:-./module/gpaw-setups.lua}")"

    if command -v wget >/dev/null 2>&1; then
        mkdir -p "$location"
        wget -O /dev/stdout "$pvalence_basis" | tar xzf - -C "$location"
        wget -O /dev/stdout "$setups" | tar xzf - -C "$location"
    elif command -v curl >/dev/null 2>&1; then
        mkdir -p "$location"
        curl "$pvalence_basis" | tar xzf - -C "$location"
        curl "$setups" | tar xzf - -C "$location"
    else
        echo Must have either wget or curl to download files
        exit 1
    fi

    mkdir -p "$(dirname "$module_file")"
    cat module/gpaw-setups.lua.template | sed "s|TEMPLATE_ROOT|$location|" > "$module_file"
    echo "Written $module_file. Don't forget to register the directory where it resides"
}

install_ase() {
    ml_conda

    ase_dir="$1"
    conda_prefix_dir="$(realpath "${2:-./conda-gpaw-env}")"

    if [ "$ase_dir" == "" ]; then
        echo conda run --prefix "$conda_prefix_dir" pip install ase
        conda run --prefix "$conda_prefix_dir" pip install ase
    else
        ase_dir="$(realpath "$ase_dir")"
        echo conda run --prefix "$conda_prefix_dir" pip install -e "$ase_dir"
        conda run --prefix "$conda_prefix_dir" pip install -e "$ase_dir"
    fi
}

install_gpaw_conda() {
    module purge
	ml_conda_buildenv

    if [ "$1" == "" ]; then
        echo "Please specify GPAW_DIR"
        exit 1
    fi

    gpaw_dir="$(realpath "$1")"
    conda_prefix_dir="$(realpath "${2:-./conda-gpaw-env}")"
    mkdir -p "$(dirname "$3")"
    module_file="$(realpath "${3:-./module/gpaw/gpaw-conda.lua}")"

    # Compile
    compile_log="$(realpath ./compile.log)"
    export GPAW_CONFIG="$(realpath ./siteconfig.py)"
    pushd "$gpaw_dir"
    rm -f _gpaw*.so
    rm -rf build
    conda run --live-stream --prefix "$conda_prefix_dir" python setup.py build_ext 2>&1 | tee "$compile_log"
    popd
    echo "Compile log saved to $compile_log"

    # Install with conda's pip
    echo conda run --prefix "$conda_prefix_dir" pip install -e "$gpaw_dir"
    conda run --prefix "$conda_prefix_dir" pip install -e "$gpaw_dir"

    cat module/gpaw/gpaw-conda.lua.template | \
        sed "s|TEMPLATE_ROOT|$gpaw_dir|" | \
        sed "s|TEMPLATE_CONDA_PREFIX|$conda_prefix_dir|" > "$module_file"
    echo "Written $module_file. Don't forget to register the directory where it resides"
}

install_git_lfs() {
    location="$(realpath "${1:-./git-lfs}")"
    url="https://github.com/git-lfs/git-lfs/releases/download/v3.0.2/git-lfs-linux-amd64-v3.0.2.tar.gz"
    mkdir -p "$(dirname "$2")"
    module_file="$(realpath "${2:-./module/git-lfs.lua}")"

    if command -v wget >/dev/null 2>&1; then
        mkdir -p "$location"
        wget -O /dev/stdout "$url" | tar xzf - -C "$location"
    elif command -v curl >/dev/null 2>&1; then
        mkdir -p "$location"
        curl "$url" | tar xzf - -C "$location"
    else
        echo Must have either wget or curl to download files
        exit 1
    fi

    mkdir -p "$(dirname "$module_file")"
    cat module/git-lfs.lua.template | sed "s|TEMPLATE_ROOT|$location|" > "$module_file"
    echo "Written $module_file. Don't forget to register the directory where it resides"
}

display_pyver() {
    module purge
    ml_conda_buildenv

    if [ "$1" == "" ]; then
        echo "Please specify CONDA_PREFIX_DIR"
        exit 1
    fi

    conda_prefix_dir="$(realpath "${1:-./conda-gpaw-env}")"
    echo -e "# Python version:\n#"
    echo "# $(get_pyver "$conda_prefix_dir")"
    echo "# $(get_pylibdir "$conda_prefix_dir")"
    echo "# $(get_py_so_abi "$conda_prefix_dir")"
}

if [ "$procedure" == "conda" ]; then
    create_conda_env "${@:2}"
elif [ "$procedure" == "gpaw-data" ]; then
    install_gpaw_data "${@:2}"
elif [ "$procedure" == "ase" ]; then
    install_ase "${@:2}"
elif [ "$procedure" == "gpaw" ]; then
    install_gpaw_conda "${@:2}"
elif [ "$procedure" == "git-lfs" ]; then
    install_git_lfs "${@:2}"
elif [ "$procedure" == "pyver" ]; then
    display_pyver "${@:2}"
else
    echo "$0 PROCEDURE [options]"
    echo """
This tool will aid you in installing software on Dardel, including

- GPAW and dependencies
- git-lfs


$0 conda [CONDA_PREFIX_DIR] [CONDA_OPTS]
  Creates a conda environment in the location CONDA_PREFIX_DIR (default ./conda-gpaw-env)
  CONDA_OPTS are options to conda create (default python=3.9 numpy=1.21.2 blas=1.0=openblas scipy=1.7.3 matplotlib)


$0 gpaw-data [LOCATION] [MODULE_FILE]
  Downloads and extracts GPAW basis sets and setups in location LOCATION (default ./gpaw-setups)
  and configures a module file MODULE_FILE (default ./module/gpaw-setups.lua)


$0 ase [ASE_DIR] [CONDA_PREFIX_DIR]
  Loads the conda environment at CONDA_PREFIX_DIR (default ./conda-gpaw-env) and installs ase in editable
  mode from the location ASE_DIR. Thus ASE_DIR should already contain the ASE repository (git clone it before).
  Leave ASE_DIR out, or specify it as the empty string "", to install it from the default pip source instead.


$0 gpaw GPAW_DIR [CONDA_PREFIX_DIR] [MODULE_FILE]
  Loads the conda environment at CONDA_PREFIX_DIR (default ./conda-gpaw-env), builds GPAW
  and pip installs it in the conda environment.
  The GPAW_DIR needs to already contain the GPAW repository (git clone it before).
  Additionally a module file MODULE_FILE (default ./module/gpaw/gpaw-conda.lua) is configured, although it is
  enough to load the conda environment (and dependent modules).

$0 git-lfs [LOCATION] [MODULE_FILE]
  Downloads and extracts the git-lfs binary in location LOCATION (default ./git-lfs)
  and configures a module file MODULE_FILE (default ./module/git-lfs.lua)
"""
fi
