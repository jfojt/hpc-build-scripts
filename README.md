# HPC build scripts

HPC build scripts and module files for GPAW

This repository includes

- Build scripts
- Module files
- Benchmark scripts

### Supported clusters

- [Dardel](https://www.pdc.kth.se/hpc-services/computing-systems/dardel-1.1043529)

## Background

[GPAW](https://wiki.fysik.dtu.dk/gpaw/) is a density-functional theory (DFT) [Python](http://www.python.org/)
code that relies on several libraries, including

- [ASE](https://wiki.fysik.dtu.dk/ase/), a pure Python package
- [libxc](https://www.tddft.org/programs/libxc/), [FFTW](http://www.fftw.org/),
  [LAPACK](https://www.netlib.org/lapack/) and [ScaLAPACK](https://www.netlib.org/scalapack/),
  which are commonly provided in optimized versions by HPC clusters

GPAW is not written purely in Python, but has crucial components that are written
in C for the sake of performance. GPAW thus needs to be compiled, and linked to
cluster-provided libraries.

### Conda

[Conda](https://docs.conda.io/en/latest/) is a package management system and
environment management system, that is commonly used by HPC clusters to supply
Python and packages as [NumPy](https://numpy.org/) and [SciPy](https://scipy.org/).

#### Conda environments

Conda environments are folders that contain all packages (Python, or other) that
are necessary for some purpose. Conda envrionments are a useful way of maintaining
multiple versions of packages without the versions interfering. Having created your
own conda environment in your home or project directory you can simply install Python
packages, as you then have the necessary file access permissions. In this regard conda
environments are similar to [virtual environments](https://docs.python.org/3/library/venv.html).
Additionally, conda environments allow one to install different versions of Python,
which virtual environments cannot.

To create a conda environment with Python version 3.9 and NumPy in your home directory

```
conda create --prefix ~/my-conda-env python=3.10 numpy
```

To activate (It might be necessary to first run `conda init`, which appends some
lines to your `.bashrc` to prepare your shell for conda)

```
conda activate ~/my-conda-env
```

Now `python` launches a `Python3.10` interpreter and NumPy is already available.
To install further packages

```
pip install ase
```

To leave the conda environment

```
conda deactivate
```

> Which environment is currently active?

```
conda info
```

**Important note:** The conda environment folders contain *a lot* of files (\~50-100k).
This can be a problem on systems where file count quotas are enforced.
Keep an eye on your disk quota.


### Module systems

Module systems, such as [Lmod](https://lmod.readthedocs.io/en/latest/),
are commonly used on clusters for providing various versions of software without
the versions interfering. The module system typically appends/removes paths from
environment variables (`$PATH`, `$PYTHONPATH`, `$LD_LIBRARY_PATH`, ...) upon loading/unloading.

## GPAW installation procedure

Go to the directory corresponding to your cluster and follow the instructions

There are essentially three ways of compiling and installing GPAW on the HPC cluster

### (Option a) Only using module files and the base Python provided by the cluster (which probably is some basic conda environment)

- With this option the installation of Python packages is cumbersome, requiring
  the modification of `$PATH` and `$PYTHONPATH`. Therefore I do not recommend this option
- Scripts and module files currently not provided, contact @jfojt for assistance

### (Option b) Creating a conda environment and using module files only for the compiled GPAW library

- Allows you to choose Python version
- `pip install` ASE and any other pure python packages as wanted
- Allows you to have multiple compiled versions (activated by different module files)
  in the same GPAW source code tree. Mutliple compiled versions (eg. serial version
  and parallel version) can be useful if the login nodes of the cluster have are
not compatible with the job nodes, and one wishes to use GPAW on the login nodes.

### (Option c) Creating a conda environment and installing GPAW purely by use of the environment

- Simplest option. In principle module files are not needed (enough to load
  the conda environment). However either way you will have to load the libxc,
  scalapack, etc.. modules so a module file can be convenient here as well.
