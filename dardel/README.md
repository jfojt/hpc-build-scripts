# Dardel

Contents of this document are:

- Installation of [GPAW](https://wiki.fysik.dtu.dk/gpaw/index.html)
- Installation of [git LFS](https://git-lfs.github.com/)
- Additional info about compilers on Dardel

## Important info about conda versions on Dardel

Currently, some Cray library on Dardel requires a GCC library in version >=
12.0. However, Conda likes to pull a lot of dependencies, and in the standard
channel it always pulls an older GCC library that then interferes with the
systemwide library. The only solution that I have found at the moment is to
force conda to use `libgcc-ng>=12.0` (only available in the conda-forge channel).
Keep this in mind when specifying some custom versions of Numpy etc. See the
defaults in `run-me.sh`.


## GPAW Installation

The installation of GPAW on Dardel is somewhat annoying because of the need to
maintain two versions.
- The serial version is compiled without MPI support and
can be used on the login nodes. This is useful if you want to do some simple
post-processing, like computing the photoabsorption spectrum.
- The parallel version is compiled with MPI support and will only work on the
  compute nodes. It will crash on the login nodes.

To be able to choose a Python version (and version of Numpy and Scipy etc..) I
recommend that a Conda environment is created and everything be installed there.
However, both the parallel and serial versions cannot simply be "pip installed"
into the Conda environment, so at least one of them will have to be "installed
by pointing the PYTHONPATH somewhere". You have a few options.

The examples below are for installation in home directory. 
I recommend to replace the home directory ~ by a path in your project directory.

### (Option a; recommended) Install serial version in Conda, and use PYTHONPATH for parallel verison

For this option, we set up a Conda environment, install ase using `pip install`
(the `./run-me.sh ase` command does this), install the serial version of GPAW 
using `pip install` as well (`./run-me.sh gpaw-serial-conda`), and then finally
compile the parallel version, but put the files somewhere else and point to them
using PYTHONPATH (done by the `./run-me.sh gpaw-parallel`) command.

**Note:** Conda will ask for confirmation before installing, so paste each line
below separately and respond to the prompt in the terminal

```
mkdir ~/git
git clone https://gitlab.com/ase/ase ~/git/ase
git clone https://gitlab.com/gpaw/gpaw ~/git/gpaw
./run-me.sh conda ~/my-conda-env
./run-me.sh gpaw-data ~/gpaw-setups ~/module/gpaw-setups.lua
./run-me.sh ase ~/git/ase ~/my-conda-env
./run-me.sh gpaw-serial-conda ~/git/gpaw ~/my-conda-env ~/module/gpaw/my-gpaw-serial.lua
./run-me.sh gpaw-parallel ~/git/gpaw ~/my-conda-env ~/module/gpaw/my-gpaw-parallel.lua
echo "module use ~/module" >> ~/.bashrc
```

**Note:** In principle the module files are not needed to use the serial version (conda
activate should be enough) but a module file can still be convenient.

### (Option b) Install both versions using PYTHONPATH

With this option, Conda will not know that GPAW is installed. The compiled files
for both serial and parallel version will be in different directories which we
will point to using PYTHONPATH.

**Note:** Conda will ask for confirmation before installing, so paste each line
below separately and respond to the prompt in the terminal

```
mkdir ~/git
git clone https://gitlab.com/ase/ase ~/git/ase
git clone https://gitlab.com/gpaw/gpaw ~/git/gpaw
./run-me.sh conda ~/my-conda-env
./run-me.sh gpaw-data ~/gpaw-setups ~/module/gpaw-setups.lua
./run-me.sh ase ~/git/ase ~/my-conda-env
./run-me.sh gpaw-serial ~/git/gpaw ~/my-conda-env ~/module/gpaw/my-gpaw-serial.lua
./run-me.sh gpaw-parallel ~/git/gpaw ~/my-conda-env ~/module/gpaw/my-gpaw-parallel.lua
echo "module use ~/module" >> ~/.bashrc
```

**Note:** You can specify which version of Python and which packages to install
to the Conda environment (run `./run-me.sh` without further arguments for help).
It seems like some versions result in Segmentation Faults. `python=3.8` and
`python=3.9` seem to work with both `blas=1.0=mkl` and `blas=*=openblas`

**Note:** Additionally, it seems like there are SegFaults if OpenMP is enabled (in siteconfig-parallel.py)

## libvdwxc installation

The following will install libvdwxc. You must first download the libvdwxc source
(here the path `~/libvdwxc` is used as example). If cloning the libvdwxc git repository
you must also run the ./autogen.sh script to generate some files needed for
compilation.

```
./run-me.sh libvdwxc ~/libvdwxc ~/module/libvdwxc.lua
echo "module use ~/module" >> ~/.bashrc
```

To link GPAW to libvdwxc you must

1. Install libvdwxc like in the above example BEFORE you install gpaw
2. Follow the instructions to install GPAW, that is create the conda environment
   and install ase
3. Append the path to libvdwxc to the command that installs gpaw
```
./run-me.sh gpaw-parallel ~/git/gpaw ~/my-conda-env ~/module/gpaw/my-gpaw-parallel.lua ~/libvdwxc
```
   OR
```
./run-me.sh gpaw-serial ~/git/gpaw ~/my-conda-env ~/module/gpaw/my-gpaw-serial.lua ~/libvdwxc
```

## Git LFS installation

The installation of Git LFS is pretty straight forward, as nothing needs to be
compiled. The installation tool will download and extract the necessary files to
the requested location.

The following downloads and installs Git LFS to `~/git-lfs` and creates the module
file `~/module/git-lfs.lua`. Adjust to your own needs

```
./run-me.sh git-lfs ~/git-lfs ~/module/git-lfs.lua
echo "module use ~/module" >> ~/.bashrc
```

## Additional info about compilers on Dardel

**NOTE**: This is now old news

On Dardel there are three suites of compilers available

```
module load PrgEnv-cray
module load PrgEnv-gnu
module load PrgEnv-aocc
```

LAPACK and ScaLAPACK are provided by

```
module load cray-libsci
```

and FFTW and MPI by

```
module load cray-fftw
module load cray-mpich
```

It seems like libxc requires
```
module load cpeGNU/21.11
module load cray-libsci
module load cray-fftw
module load cray-mpich
module load libxc/5.1.7-cpeGNU-21.11
```

About cpeGNU:

> The module has the same effect as loading PrgEnv-gnu and cpe should have, but
> does not use either of those modules. The use of PrgEnv-gnu is avoided since
> the modules that it loads may change over time, reducing reproducibility.
