#!/bin/bash

procedure="$1"

die() {
    echo $@
    exit 1
}

get_pyver() {
    conda_prefix_dir="$1"
    pycmd="""
import sys
import sysconfig

plat = sysconfig.get_platform()
vi = sys.version_info  # sysconfig.get_python_version seems to give same as vi[0].vi[1]

# I can't wrap my head around this..
# The following gives something like 'linux-x86_64-3.10' which sometimes is the correct 'build_dir'
platstr = f'{plat}-{vi[0]}.{vi[1]}'
# Other times, the following is needed, which is something like 'linux-x86_64-cpython-310'
platstr = '-'.join([sysconfig.get_platform()] + sysconfig.get_config_var('SOABI').split('-')[:2])
# When something does not work: check the output of the compilation. Look for 'build lib: ...'
print(platstr, end='')
"""
    conda run --prefix "$conda_prefix_dir" python -c "$pycmd"
}

create_conda_env() {
    module load systemdefault/1.0.0
    module load PDC/23.12
    module load anaconda3/2024.02-1-cpeGNU-23.12

    echo Creating conda environment
    conda_prefix_dir="$(realpath "${1:-./conda-gpaw-env}")"
    conda_opts="${@:2}"
    conda_opts="${conda_opts:--c conda-forge python==3.10.14 numpy==1.24.3 blas=*=openblas libgcc-ng>=12 scipy==1.10.1}"

    echo conda create --prefix "$conda_prefix_dir" $conda_opts
    conda create --prefix "$conda_prefix_dir" $conda_opts
}

install_gpaw_data() {
    location="$(realpath "${1:-./gpaw-setups}")"
    pvalence_basis="https://wiki.fysik.dtu.dk/gpaw-files/gpaw-basis-pvalence-0.9.20000.tar.gz"
    setups="https://wiki.fysik.dtu.dk/gpaw-files/gpaw-setups-0.9.20000.tar.gz"
    mkdir -p "$(dirname "$2")"
    module_file="$(realpath "${2:-./module/gpaw-setups.lua}")"

    if command -v wget >/dev/null 2>&1; then
        mkdir -p "$location"
        wget -O /dev/stdout "$pvalence_basis" | tar xzf - -C "$location"
        wget -O /dev/stdout "$setups" | tar xzf - -C "$location"
    elif command -v curl >/dev/null 2>&1; then
        mkdir -p "$location"
        curl "$pvalence_basis" | tar xzf - -C "$location"
        curl "$setups" | tar xzf - -C "$location"
    else
        echo Must have either wget or curl to download files
        exit 1
    fi

    mkdir -p "$(dirname "$module_file")"
    cat module/gpaw-setups.lua.template | sed "s|TEMPLATE_ROOT|$location|" > "$module_file"
    echo "Written $module_file. Don't forget to register the directory where it resides"
}

install_ase() {
    module load systemdefault/1.0.0
    module load PDC/23.12
    module load anaconda3/2024.02-1-cpeGNU-23.12
    #
    # Init conda
    eval "$('/pdc/software/23.12/eb/software/anaconda3/2024.02-1-cpeGNU-23.12/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"

    ase_dir="$1"
    conda_prefix_dir="$(realpath "${2:-./conda-gpaw-env}")"

    if [ "$ase_dir" == "" ]; then
        echo conda run --prefix "$conda_prefix_dir" pip install ase
        conda run --prefix "$conda_prefix_dir" pip install ase
    else
        ase_dir="$(realpath "$ase_dir")"
        echo conda run --prefix "$conda_prefix_dir" pip install -e "$ase_dir"
        conda run --prefix "$conda_prefix_dir" pip install -e "$ase_dir"
    fi
}

install_gpaw_serial() {
    # module --force purge
    # export CRAY_PE_LIBSCI_PREFIX_DIR=/opt/cray/pe/libsci/23.12.5/CRAYCLANG/17.0/x86_64
    # source /opt/cray/pe/cpe/23.12/restore_lmod_system_defaults.sh

    module load systemdefault/1.0.0
    module load PDC/23.12
    module load anaconda3/2024.02-1-cpeGNU-23.12
    module load libxc/6.0.0

    # Init conda
    eval "$('/pdc/software/23.12/eb/software/anaconda3/2024.02-1-cpeGNU-23.12/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"

    if [ "$1" == "" ]; then
        echo "Please specify GPAW_DIR"
        exit 1
    fi

    gpaw_dir="$(realpath "$1")"
    conda_prefix_dir="$(realpath "${2:-./conda-gpaw-env}")"
    mkdir -p "$(dirname "$3")"
    module_file="$(realpath "${3:-./module/gpaw/serial.lua}")"
    if [ "$4" != "" ]; then
        libvdwxc_dir="$(realpath "$4")"
        echo "Enabling libvdwxc. Path: $libvdwxc_dir"
        export LIBVDWXC_PATH="$libvdwxc_dir"
    fi

    # Back up whatever is in build folder
    if [ -e "$gpaw_dir/build" ]; then
        mv "$gpaw_dir/build" "$gpaw_dir/build.hpc-build-scipts-tmp-bak"
    fi

    # Compile
    compile_log="./compile-serial.log"
    export GPAW_CONFIG="$(realpath ./siteconfig-serial.py)"
    pushd "$gpaw_dir"
    conda run --prefix "$conda_prefix_dir" python setup.py build_ext 2>&1 | tee "$compile_log"
    popd
    if [ -e "$gpaw_dir/build" ]; then
        rm -rf "$gpaw_dir/build_serial"
        mv "$gpaw_dir/build" "$gpaw_dir/build_serial"
    fi
    echo "Compile log saved to $compile_log"

    # Make so that conda can access the GPAW scripts
    echo conda develop --prefix "$conda_prefix_dir" "$gpaw_dir"
    conda develop --prefix "$conda_prefix_dir" "$gpaw_dir"
    echo "To uninstall from this conda environment, do conda develop --prefix $conda_prefix_dir -u $gpaw_dir"

    # Restore whatever is in build folder
    if [ -e "$gpaw_dir/build.hpc-build-scipts-tmp-bak" ]; then
        mv "$gpaw_dir/build.hpc-build-scipts-tmp-bak" "$gpaw_dir/build"
    fi

    pyver="$(get_pyver "$conda_prefix_dir")"

    cat module/gpaw/serial.lua.template | \
        sed "s|TEMPLATE_ROOT|$gpaw_dir|" | \
        sed "s|TEMPLATE_CONDA_PREFIX|$conda_prefix_dir|" | \
        sed "s/TEMPLATE_PYTHON_PLATFORM/$pyver/" > "$module_file"
    echo "Written $module_file. Don't forget to register the directory where it resides"
}

install_gpaw_parallel() {
    module load systemdefault/1.0.0
    module load PDC/23.12
    module load anaconda3/2024.02-1-cpeGNU-23.12
    module load libxc/6.0.0
    module load cray-fftw/3.3.10.6

    # Init conda
    eval "$('/pdc/software/23.12/eb/software/anaconda3/2024.02-1-cpeGNU-23.12/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"

    if [ "$1" == "" ]; then
        echo "Please specify GPAW_DIR"
        exit 1
    fi

    gpaw_dir="$(realpath "$1")"
    conda_prefix_dir="$(realpath "${2:-./conda-gpaw-env}")"
    mkdir -p "$(dirname "$3")"
    module_file="$(realpath "${3:-./module/gpaw/parallel.lua}")"
    if [ "$4" != "" ]; then
        libvdwxc_dir="$(realpath "$4")"
        echo "Enabling libvdwxc. Path: $libvdwxc_dir"
        export LIBVDWXC_PATH="$libvdwxc_dir"
    fi

    # Back up whatever is in build folder
    if [ -e "$gpaw_dir/build" ]; then
        mv "$gpaw_dir/build" "$gpaw_dir/build.hpc-build-scipts-tmp-bak"
    fi

    # Compile
    compile_log="$(realpath ./compile-parallel.log)"
    export GPAW_CONFIG="$(realpath ./siteconfig-parallel.py)"
    pushd "$gpaw_dir"

    conda run --prefix "$conda_prefix_dir" python setup.py build_ext 2>&1 | tee "$compile_log"
    popd
    if [ -e "$gpaw_dir/build" ]; then
        rm -rf "$gpaw_dir/build_parallel"
        mv "$gpaw_dir/build" "$gpaw_dir/build_parallel"
    fi
    echo "Compile log saved to $compile_log"

    # Make so that conda can access the GPAW scripts
    echo conda develop --prefix "$conda_prefix_dir" "$gpaw_dir"
    conda develop --prefix "$conda_prefix_dir" "$gpaw_dir"
    echo "To uninstall from this conda environment, do conda develop --prefix $conda_prefix_dir -u $gpaw_dir"

    # Restore whatever is in build folder
    if [ -e "$gpaw_dir/build.hpc-build-scipts-tmp-bak" ]; then
        mv "$gpaw_dir/build.hpc-build-scipts-tmp-bak" "$gpaw_dir/build"
    fi

    pyver="$(get_pyver "$conda_prefix_dir")"
    echo $pyver

    cat module/gpaw/parallel.lua.template | \
        sed "s|TEMPLATE_ROOT|$gpaw_dir|" | \
        sed "s|TEMPLATE_CONDA_PREFIX|$conda_prefix_dir|" | \
        sed "s/TEMPLATE_PYTHON_PLATFORM/$pyver/" > "$module_file"
    echo "Written $module_file. Don't forget to register the directory where it resides"
}

install_gpaw_serial_conda() {
    module load systemdefault/1.0.0
    module load PDC/23.12
    module load anaconda3/2024.02-1-cpeGNU-23.12
    module load libxc/6.0.0

    # Init conda
    eval "$('/pdc/software/23.12/eb/software/anaconda3/2024.02-1-cpeGNU-23.12/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"

    if [ "$1" == "" ]; then
        echo "Please specify GPAW_DIR"
        exit 1
    fi

    gpaw_dir="$(realpath "$1")"
    conda_prefix_dir="$(realpath "${2:-./conda-gpaw-env}")"
    mkdir -p "$(dirname "$3")"
    module_file="$(realpath "${3:-./module/gpaw/serial-conda.lua}")"
    if [ "$4" != "" ]; then
        libvdwxc_dir="$(realpath "$4")"
        echo "Enabling libvdwxc. Path: $libvdwxc_dir"
        export LIBVDWXC_PATH="$libvdwxc_dir"
    fi

    # Compile
    compile_log="$(realpath ./compile-serial.log)"
    export GPAW_CONFIG="$(realpath ./siteconfig-serial.py)"
    pushd "$gpaw_dir"
    conda run --prefix "$conda_prefix_dir" python setup.py build_ext 2>&1 | tee "$compile_log"
    popd
    echo "Compile log saved to $compile_log"

    # Install with conda's pip
    echo conda run --prefix "$conda_prefix_dir" pip install -e "$gpaw_dir"
    conda run --prefix "$conda_prefix_dir" pip install -e "$gpaw_dir"

    cat module/gpaw/serial-conda.lua.template | \
        sed "s|TEMPLATE_ROOT|$gpaw_dir|" | \
        sed "s|TEMPLATE_CONDA_PREFIX|$conda_prefix_dir|" > "$module_file"
    echo "Written $module_file. Don't forget to register the directory where it resides"
}

install_gpaw_parallel_conda() {
    module load systemdefault/1.0.0
    module load PDC/23.12
    module load anaconda3/2024.02-1-cpeGNU-23.12
    module load libxc/6.0.0
    module load cray-fftw/3.3.10.6

    # Init conda
    eval "$('/pdc/software/23.12/eb/software/anaconda3/2024.02-1-cpeGNU-23.12/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"

    if [ "$1" == "" ]; then
        echo "Please specify GPAW_DIR"
        exit 1
    fi

    if [ "$1" == "" ]; then
        echo "Please specify GPAW_DIR"
        exit 1
    fi

    gpaw_dir="$(realpath "$1")"
    conda_prefix_dir="$(realpath "${2:-./conda-gpaw-env}")"
    mkdir -p "$(dirname "$3")"
    module_file="$(realpath "${3:-./module/gpaw/parallel-conda.lua}")"
    if [ "$4" != "" ]; then
        libvdwxc_dir="$(realpath "$4")"
        echo "Enabling libvdwxc. Path: $libvdwxc_dir"
        export LIBVDWXC_PATH="$libvdwxc_dir"
    fi

    # Compile
    compile_log="$(realpath ./compile-parallel.log)"
    export GPAW_CONFIG="$(realpath ./siteconfig-parallel.py)"
    pushd "$gpaw_dir"
    conda run --prefix "$conda_prefix_dir" python setup.py build_ext 2>&1 | tee "$compile_log"
    popd
    echo "Compile log saved to $compile_log"

    # Install with conda's pip
    echo conda run --prefix "$conda_prefix_dir" pip install -e "$gpaw_dir"
    conda run --prefix "$conda_prefix_dir" pip install -e "$gpaw_dir"

    cat module/gpaw/parallel-conda.lua.template | \
        sed "s|TEMPLATE_ROOT|$gpaw_dir|" | \
        sed "s|TEMPLATE_CONDA_PREFIX|$conda_prefix_dir|" > "$module_file"
    echo "Written $module_file. Don't forget to register the directory where it resides"
}

install_libvdwxc() {
    module load PDC/23.12
    module load cpeGNU/22.06
    module load cray-libsci
    module load cray-fftw/3.3.10.6
    module load cray-mpich
    module load libxc/6.0.0
    module load cpe/22.06

    if [ "$1" == "" ]; then
        echo "Please specify LIBVDWXC_DIR"
        exit 1
    fi

    libvdwxc_dir="$(realpath "$1")"
    mkdir -p "$(dirname "$2")"
    module_file="$(realpath "${2:-./module/libvdwxc.lua}")"

    # Create and empty build folder
    build_dir="$libvdwxc_dir/build-hcp-build-scripts"
    mkdir -p "$build_dir"
    rm -rf "$build_dir/*"

    # Compile
    # cc is the wrapper for the mpi enabled c compiler
    # ftn is the wrapper for the mpi enabled fortran compiler
    compile_log="$(realpath ./compile.log)"
    pushd "$build_dir"
    ../configure --prefix="$libvdwxc_dir" CC="cc" \
          FC="ftn" CFLAGS="-O3 -march=native" FCFLAGS="-g -O2" | tee "$compile_log" || die "Configure failed"
    make -j4 | tee "$compile_log" || die "Compile failed"
    make check | tee "$compile_log" || die "Tests"

    # Remove old installation
    rm -rf "$libvdwxc_dir/bin" "$libvdwxc_dir/include" "$libvdwxc_dir/lib"

    make install | tee "$compile_log" || die "Install failed"

    popd
    echo "Compile log saved to $compile_log"

    # Install module
    cat module/libvdwxc.lua.template | \
        sed "s|TEMPLATE_ROOT|$libvdwxc_dir|" > "$module_file"
    echo "Written $module_file. Don't forget to register the directory where it resides"
}

install_git_lfs() {
    location="$(realpath "${1:-./git-lfs}")"
    url="https://github.com/git-lfs/git-lfs/releases/download/v3.0.2/git-lfs-linux-amd64-v3.0.2.tar.gz"
    mkdir -p "$(dirname "$2")"
    module_file="$(realpath "${2:-./module/git-lfs.lua}")"

    if command -v wget >/dev/null 2>&1; then
        mkdir -p "$location"
        wget -O /dev/stdout "$url" | tar xzf - -C "$location"
    elif command -v curl >/dev/null 2>&1; then
        mkdir -p "$location"
        curl "$url" | tar xzf - -C "$location"
    else
        echo Must have either wget or curl to download files
        exit 1
    fi

    mkdir -p "$(dirname "$module_file")"
    cat module/git-lfs.lua.template | sed "s|TEMPLATE_ROOT|$location|" > "$module_file"
    echo "Written $module_file. Don't forget to register the directory where it resides"
}


if [ "$procedure" == "conda" ]; then
    create_conda_env "${@:2}"
elif [ "$procedure" == "gpaw-data" ]; then
    install_gpaw_data "${@:2}"
elif [ "$procedure" == "ase" ]; then
    install_ase "${@:2}"
elif [ "$procedure" == "gpaw-serial" ]; then
    install_gpaw_serial "${@:2}"
elif [ "$procedure" == "gpaw-parallel" ]; then
    install_gpaw_parallel "${@:2}"
elif [ "$procedure" == "gpaw-serial-conda" ]; then
    install_gpaw_serial_conda "${@:2}"
elif [ "$procedure" == "gpaw-parallel-conda" ]; then
    install_gpaw_parallel_conda "${@:2}"
elif [ "$procedure" == "libvdwxc" ]; then
    install_libvdwxc "${@:2}"
elif [ "$procedure" == "git-lfs" ]; then
    install_git_lfs "${@:2}"
else
    echo "$0 PROCEDURE [options]"
    echo """
This tool will aid you in installing software on Dardel, including

- GPAW and dependencies
- git-lfs


$0 conda [CONDA_PREFIX_DIR] [CONDA_OPTS]
  Creates a conda environment in the location CONDA_PREFIX_DIR (default ./conda-gpaw-env)
  CONDA_OPTS are options to conda create (default python=3.10 numpy=1.24.3 blas=*=openblas scipy=1.10.1)


$0 gpaw-data [LOCATION] [MODULE_FILE]
  Downloads and extracts GPAW basis sets and setups in location LOCATION (default ./gpaw-setups)
  and configures a module file MODULE_FILE (default ./module/gpaw-setups.lua)


$0 ase [ASE_DIR] [CONDA_PREFIX_DIR]
  Loads the conda environment at CONDA_PREFIX_DIR (default ./conda-gpaw-env) and installs ase in editable
  mode from the location ASE_DIR. Thus ASE_DIR should already contain the ASE repository (git clone it before).
  Leave ASE_DIR out, or specify it as the empty string "", to install it from the default pip source instead.


$0 gpaw-serial GPAW_DIR [CONDA_PREFIX_DIR] [MODULE_FILE] [LIBVDWXC_DIR]
  Loads the conda environment at CONDA_PREFIX_DIR (default ./conda-gpaw-env), builds GPAW in serial mode
  (for use on the login node), moves the built files to GPAW_DIR/build_serial, and configures a module file
  MODULE_FILE (default ./module/gpaw/serial.lua) to load the serial build of gpaw.
  The GPAW_DIR needs to already contain the GPAW repository (git clone it before).
  Optional: Specify the directory with the installation of libvdwxc LIBVDWXC_DIR


$0 gpaw-parallel GPAW_DIR [CONDA_PREFIX_DIR] [MODULE_FILE] [LIBVDWXC_DIR]
  Loads the conda environment at CONDA_PREFIX_DIR (default ./conda-gpaw-env), builds GPAW in parallel mode
  (for use in MPI on the compute nodes), moves the built files to GPAW_DIR/build_parallel, and configures a
  MODULE_FILE (default ./module/gpaw/parallel.lua) module file to load the parallel build of gpaw.
  The GPAW_DIR needs to already contain the GPAW repository (git clone it before).
  Optional: Specify the directory with the installation of libvdwxc LIBVDWXC_DIR


$0 gpaw-serial-conda GPAW_DIR [CONDA_PREFIX_DIR] [MODULE_FILE] [LIBVDWXC_DIR]
  Loads the conda environment at CONDA_PREFIX_DIR (default ./conda-gpaw-env), builds GPAW in serial mode
  (for use on the login node), and pip installs it in the conda environment.
  The GPAW_DIR needs to already contain the GPAW repository (git clone it before).
  This option must *not* be used together with gpaw-parallel-conda *for the same* GPAW_DIR. Use different GPAW_DIRs
  Additionally a module file MODULE_FILE (default ./module/gpaw/serial-conda.lua) is configured, although it is
  enough to load the conda environment (and dependent modules).
  Optional: Specify the directory with the installation of libvdwxc LIBVDWXC_DIR


$0 gpaw-parallel-conda GPAW_DIR [CONDA_PREFIX_DIR] [MODULE_FILE] [LIBVDWXC_DIR]
  Loads the conda environment at CONDA_PREFIX_DIR (default ./conda-gpaw-env), builds GPAW in parallel mode
  (for use in MPI on the compute nodes), and pip installs it in the conda environment.
  The GPAW_DIR needs to already contain the GPAW repository (git clone it before).
  This option must *not* be used together with gpaw-serial-conda *for the same* GPAW_DIR. Use different GPAW_DIRs
  Additionally a module file MODULE_FILE (default ./module/gpaw/parallel-conda.lua) is configured, although it is
  enough to load the conda environment (and dependent modules).
  Optional: Specify the directory with the installation of libvdwxc LIBVDWXC_DIR

$0 git-lfs [LOCATION] [MODULE_FILE]
  Downloads and extracts the git-lfs binary in location LOCATION (default ./git-lfs)
  and configures a module file MODULE_FILE (default ./module/git-lfs.lua)

$0 libvdwxc LIBVDWXC_DIR [MODULE_FILE]
  Builds libvdwxc, installs to LIBVDWXC_DIR, and configures a MODULE_FILE (default ./module/libvdwxc.lua)
  module file to load the library.
  The LIBVDWXC_DIR needs to already contain the libvdwxc source tree (git clone and ./autogen.sh it before).
"""
fi
