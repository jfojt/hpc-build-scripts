#!/bin/bash -l
#SBATCH -A snicXXXX-Y-ZZZ
#SBATCH -J my-job
#SBATCH -t 01:00:00
#SBATCH --nodes=1
#SBATCH -p main
#SBATCH --ntasks-per-node=128

# These environment variables are convenient if we want to know how much time is left on the job

export START_TIME=`date +%s.%3N`
export TIME_LIMIT=`squeue -h -o %l -j $SLURM_JOB_ID`

module load gpaw/parallel

srun gpaw test
