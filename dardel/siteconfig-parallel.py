"""User provided customizations.

Here one changes the default arguments for compiling _gpaw.so.

Here are all the lists that can be modified:

* libraries
  List of libraries to link: -l<lib1> -l<lib2> ...
* library_dirs
  Library search directories: -L<dir1> -L<dir2> ...
* include_dirs
  Header search directories: -I<dir1> -I<dir2> ...
* extra_link_args
  Arguments forwarded directly to linker
* extra_compile_args
  Arguments forwarded directly to compiler
* runtime_library_dirs
  Runtime library search directories: -Wl,-rpath=<dir1> -Wl,-rpath=<dir2> ...
* extra_objects
* define_macros

The following lists work like above, but are only linked when compiling
the parallel interpreter:

* mpi_libraries
* mpi_library_dirs
* mpi_include_dirs
* mpi_runtime_library_dirs
* mpi_define_macros

To override use the form:

    libraries = ['somelib', 'otherlib']

To append use the form

    libraries += ['somelib', 'otherlib']
"""
from os import getenv

# flake8: noqa

# cc is a Wrapper on Dardel for the appropriate compiler
compiler = 'cc'
# CAUTION: In newer versions of GPAW, set mpi = True and compiler = '...' for MPI
# In older versions, set mpicompiler = ... and mpilinker = ... for MPI (None to disable)
mpi = True
# platform_id = ''

extra_compile_args += ['-O3', '-march=znver2', '-mtune=znver2', '-mfma', '-mavx2',
                       '-m3dnow', '-fomit-frame-pointer']

# OpenMP
# This seems to result in some segfault on interpreter shutdown
# extra_compile_args += ['-fopenmp']
# extra_link_args += ['-fopenmp']

# FFTW3:

fftw = True
if fftw:
    libraries += ['fftw3']

# ScaLAPACK (version 2.0.1+ required):
scalapack = True
if scalapack:
    libraries += ['sci_gnu']

# Use Elpa (requires ScaLAPACK and Elpa API 20171201):
if 0:
    elpa = True
    elpadir = '/home/user/elpa'
    libraries += ['elpa']
    library_dirs += ['{}/lib'.format(elpadir)]
    extra_link_args += ['-Wl,-rpath={}/lib'.format(elpadir)]
    include_dirs += ['{}/include/elpa-xxxx.xx.xxx'.format(elpadir)]

# LibXC:
# In order to link libxc installed in a non-standard location
# (e.g.: configure --prefix=/home/user/libxc-2.0.1-1), use:

# - static linking:
if False:
    xc = getenv('EBROOTLIBXC')
    include_dirs += [xc + '/include']
    extra_link_args += [xc + '/lib/libxc.a']
    if 'xc' in libraries:
        libraries.remove('xc')

# - dynamic linking (requires rpath or setting LD_LIBRARY_PATH at runtime):
if True:
    xc = getenv('EBROOTLIBXC')
    include_dirs += [xc + '/include']
    library_dirs += [xc + '/lib']
    if 'xc' not in libraries:
        libraries.append('xc')


# libvdwxc:
libvdwxc_path = getenv('LIBVDWXC_PATH', None)
if libvdwxc_path is not None:
    libvdwxc = True
    extra_link_args += [f'-Wl,-rpath={libvdwxc_path}/lib']
    library_dirs += [f'{libvdwxc_path}/lib']
    include_dirs += [f'{libvdwxc_path}/include']
    libraries += ['vdwxc']
